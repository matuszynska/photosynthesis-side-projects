#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 16:19:31 2020

@author: anna
"""
   
#Function for PAM experiment
def changingLight(model, y0d, PFD=125., dark =10.):
    tprot = np.array([  1.        ,   0.8       ,  5.86666667,   0.8       ,
                  15.86666667,   0.8       ,  30.53333333,   0.8       ,
                  30.2       ,   0.8       ,  30.2       ,   0.8       ,
                  30.86666667,   0.8       , 30.2       ,   0.8       ,
                  30.53333333,   0.8       , 30.,   0.8       ,
                  30.       ,   0.8       ,  30.2       ,   0.8       ,
                  30.       ,   0.8       ,  30.2       ,   0.8       ,
                  15.53333333,   0.8       ,  30.2       ,   0.8       ,
                  30.53333333,   0.8       , 30.53333333,   0.8       ,
                  30.2       ,   0.8       ,  30.86666667,   0.8       ,
                  30.86666667,   0.8       ,  30.53333333,   0.8       ,
                  30.86666667,   0.8       ,  30.53333333,   0.8       ,
                  30.86666667,   0.8       ,  
                  30.53333333,   0.8       , 30.86666667,   0.8  ,
                                    30.86666667,   0.8       ,  
                  30.53333333,   0.8       , 30.86666667,   0.8 ])
    
    ProtPFDs = np.array([   0.   , 5000.   ,    PFD   , 5000.   ,  
                     PFD, 5000.   , PFD, 5000.   ,  
                     PFD, 5000.   ,  PFD, 5000.   ,
                     PFD, 5000.   ,  PFD, 5000.   ,  PFD, 5000.   ,
                     PFD, 5000.   ,    PFD  , 5000.   ,    PFD  , 5000.   ,
                    PFD  , 5000.   ,   PFD  , 5000.   ,   dark   , 5000.   ,
                    dark , 5000.   ,    dark  , 5000.   ,
                    dark , 5000.   ,    dark  , 5000.   ,  dark, 5000.   ,
                   dark, 5000.   ,  dark, 5000.   ,  dark, 5000.   ,
                   dark, 5000.   ,  dark, 5000.   ,  dark, 5000.   ,
                                      dark, 5000.   ,  dark, 5000.   ,  dark, 5000.   ,
                     dark, 5000.   ])
    
    
    s = Simulator(model)
    s.initialise(y0d)
    dt = 0
    for i in range(len(tprot)):
        s.update_parameter('pfd', ProtPFDs[i])
        dt += tprot[i]
        s.simulate(dt, **{"rtol":1e-14,"atol":1e-10, "maxnef": 20, "maxncf":10})
    return s


def changingLightOnlyLight(model, y0d, PFD=125., dark =10.):
    tprot = np.array([  1.        ,   0.8       ,  5.86666667,   0.8       ,
                  15.86666667,   0.8       ,  30.53333333,   0.8       ,
                  30.2       ,   0.8       ,  30.2       ,   0.8       ,
                  30.86666667,   0.8       , 30.2       ,   0.8       ,
                  30.53333333,   0.8       , 30.,   0.8       ,
                  30.       ,   0.8       ,  30.2       ,   0.8       ,
                  30.       ,   0.8       ,  30.2       ,   0.8   ])
    
    ProtPFDs = np.array([   0.   , 5000.   ,    PFD   , 5000.   ,  
                     PFD, 5000.   , PFD, 5000.   ,  
                     PFD, 5000.   ,  PFD, 5000.   ,
                     PFD, 5000.   ,  PFD, 5000.   ,  PFD, 5000.   ,
                     PFD, 5000.   ,    PFD  , 5000.   ,    PFD  , 5000.   ,
                    PFD  , 5000.   ,   PFD  , 5000.
                    ])
    
    
    s = Simulator(model)
    s.initialise(y0d)
    dt = 0
    for i in range(len(tprot)):
        s.update_parameter('pfd', ProtPFDs[i])
        dt += tprot[i]
        s.simulate(dt, **{"rtol":1e-14,"atol":1e-10, "maxnef": 20, "maxncf":10})
    return s    