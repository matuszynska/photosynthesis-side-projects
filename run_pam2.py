#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 16:29:06 2020

@author: anna
"""
#y0 =  {"P": 0., "H": 5.56305280e-04, "E": 0.5, "A": 1., "Lhcx":.3, "Dd": 1.}
#
#s = Simulator(M16model)
#s.initialise(y0)
#s.update_parameter('pfd', 0.)
#s.simulate(1000, steps=2000, **{"atol":1e-8})
#
#
#s.plot_selection(['Dt'])
#plt.plot(s.get_time(), s.get_variable('pH'), 'g')
#ax = plt.gca()
#ax.annotate('lumenal pH', xy=(600, 7.2), color='g')
#s.plot_fluxes('vXcyc')
#
#y0d = s.get_results_array()[-1]

colpal = {'Mutant 1': 'limegreen',
          'Mutant 2': 'darkgreen',
          'WT': 'red',
          'Overexpressor 1': 'blue',
}
#{'WT': 1000.,}
leak_var = {'Mutant 1': 3500.,
            'Mutant 2': 3000.,
            'WT': 4500.,
            'Overexpressor 1': 5000.,
           }

M16model.update_parameters({'kH0': 5e8,
                            'kH': 5e9,     # paramter relating the rate constant of activation of the ATPase in the light
                            'kDeactATPase': 0.002, 
                            'kF': 6.25e8,              # fluorescence 16ns
                            'kP': 2.5e9,     })

M16model.update_parameter('gamma0', 1.)
M16model.update_parameter('kActATPase', 0.001)
M16model.update_parameter('kDeepox', 0.04)
M16model.update_parameter('kEpox', 0.05)


y0 =  {"P": 0., "H": 5.56305280e-04, "E": 0.5, "A": 1., "Lhcx":.3, "Dd": 1.}

fig3, axdt2 = plt.subplots()
for light in [20, 25, 30, 35, 40,50,60,70,80,90,100]:

    for leak in leak_var.keys():
    
        M16model.update_parameter('kleak', leak_var[leak])
        
        
        s = Simulator(M16model)
        s.initialise(y0)
        s.update_parameters({'pfd': 0.,
                             'kleak': leak_var[leak]})
        s.simulate(1000, steps=2000, **{"atol":1e-8})
        y0d = s.get_results_array()[-1]
    
            
        PAM1 = changingLightOnlyLight(M16model, y0d, PFD=light, dark =10.)
        F = PAM1.get_variable('Fluo')
        Fm, NPQ, tm, Fo, to = get_NPQ(PAM1.get_variable('Fluo'), PAM1.get_time(), PAM1.get_variable('L'), 5000)
            
        axdt2.scatter(light, NPQ[-1], marker='s', color=colpal[leak])
    
plt.legend()    
plt.show()