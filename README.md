# Update 15 Oct 2020
Implementation of the concept of partitioning of protons. KEA3 mutant will affect NPQ, but not ATP productivity. **Commit:** ed1a59e56a2a4b8b24de84127e76572fdae2987a (new branch)
- Ha -> ATP, PQox
- Hb -> NPQ (deepox, epox, prot)
- dynamics of protons as before
- new algebraic module to calculate H_beta

Results for NPQ ML. Ratios proposed by Claire are higher. 

![hbeta npq](/images/NPQ_ML_partitioning.png "Calculated NPQ traces for the ML light with different representation of the mutant")

![zx vs npq](/images/NPQvsZx.png "Linear dependency between the NPQ and [Zx]")

# Update 8 Oct 2020
Implementation of artificial limiting rate of ATPsynthase. **Commit:** bedee0ff87d7c0f343b80975f2d9cdbae39af09e

![limited npq](images/NPQ_corrections.png "npq of model with limited ATPproduction")

follows the bell-shaped light dependence curve experimentally observed by Claire

![limited bell](images/bell-shape.png "light scan of model with limited ATPproduction")


# Simplified mathematical model of the NPQ in DIATOMS
 
With this work I aim to reproduce the behaviour of KEA3 mutants, observed by Claire and Giovanni testing various possible mechanisms of quenching in diatoms.

I have been comparing the simulated fluorescence traces to this experimental data:

![raw fluo](/images/Fluo_raw_exp.png "Raw fluorescence traces")

The aim of the task was to find the mechanism that will reproduce this behaviour, checking the hypothesis on the linear relationship between the NPQ value and deepoxidised xantophylls.

![raw npq](/images/NPQ_raw_exp.png "Calculated NPQ traces")


#  Model
The model is based on the ETC model for Arabidopsis, as described in Matuszynska et al. 2016 [1].

[1] Matuszyńska, A., Heidari, S., Jahns, P., Ebenhöh, O., 2016. A mathematical model of non-photochemical quenching to study short-term light memory in plants. Biochimica et Biophysica Acta (BBA) - Bioenergetics 1857, 1860–1869. https://doi.org/10.1016/j.bbabio.2016.09.003)

### Results
NPQ for the pfd=125 was normalized to the max NPQ obtained for the simulations with the pfd=1250 and was =2.
Raw results of the simulation for the pfd=1250 are below
![raw pfd](/images/HL_WT.png "[Dt]% vs NPQ normalized to the max NPQ for pfd=1250")

Below I've plotted how the NPQ for HL looked for all mutants. I could not reproduce the huge effect in the slower relaxation of the NPQ for the KEA3 mutants
![NPQ for HL-LL](/images/NPQ_HL-LL.png "NPQ normalized to the max NPQ for pfd=1250")

Below the raw simulation results including the fluorescence trace (I am very satisfied with the fluorescence kinetics) for WT, mutants and overexpressors.
*WILD TYPE*
![WT ML-LL](/images/ML_WT.png "ML wild type")
*kea3 mutants*
![kea3-1 ML-LL](/images/ML_kea3-1.png "ML kea3 mutant")
![kea3-2 ML-LL](/images/ML_kea3-2.png "ML kea3 second mutant")
*kea3 overexpressors*
![kea3-overexp1 ML-LL](/images/ML_kea3-overexp1.png "ML kea3 overexp1")
![kea3-overexp2 ML-LL](/images/ML_kea3-overexp2.png "ML kea3 second overexp1")

Together summarised on this figure:
![NPQ for ML-LL](/images/NPQ_ML-LL.png "NPQ normalized to the max NPQ for pfd=125")

Finally, in this figure the concentration of Dt has been plotted vs the NPQ (also normalized.)
![DEs vs NPQ](/images/ML_DES.png "[Dt]% vs NPQ normalized to the max NPQ for pfd=1250")



### Assumptions
To account for the dynamics of diatoms we focus only on:
- PSII, H for pH and pmf, ATP and xantophylls.
(The overall change in the lumenal pH depends on the rate of photosystem II, cytochrome b6f, ATP synthase and proton leak)
- after testing only Dt dependent quencher model, various models of cooperation and addition have been tested with the final being $\gamma \cdot [Dt] \cdot [Lhcx^P]$ 
- deepoxidation follows Hill kinetics, epoxidation governed by the mass action,
- KEA3 has been simulated by changing the reaction rate of the leakage.

### Changes to the model
- the mid point potential from 0.380 to 0.350
- Fv/Fm of a diatom is lower than of the plant, ranging between 0.6-0.7. Hence the charge separation rate constants needed to differ. To obtain the Fv/Fm=0.68 I've assumed:
    - unchanged base quencher (kH0),
    - unchanged rate of induced quencher (kH),
    - unchanged fluoresnce rate (kF),
    - lower charge separation (kP used to be 5e9, now 1.2e9)
`fvfm = M16model.get_parameter('kP')/(M16model.get_parameter('kF') + M16model.get_parameter('kP') + M16model.get_parameter('kH0') + M16model.get_parameter('kH')*Q)`
- lumen and stroma volumes are in diatoms similar. Therefore lumen is much bigger than we assumed before and the accumulated protons will have slower effect than before. I have changed the fuction calculating the lumenal pH from the H concentration to account for the bigger lumen. Ideally, I should have also follow now dynamically the concentration of stromal H, but I have obtained already good qualitative agreement,
- rate constants of the deepoxidation and epoxidation are of the same range.
